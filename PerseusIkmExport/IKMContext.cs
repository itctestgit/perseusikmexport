﻿using System.Configuration;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace PerseusIkmExport
{
    [Database]
    internal class IkmContext : DataContext
    {
        public Table<ExportRecord> TimeLists;

        public IkmContext()
            : base(ConfigurationManager.AppSettings["TargetConnectionString"])
        {
        }
    }
}