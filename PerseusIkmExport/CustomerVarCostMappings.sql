CREATE TABLE [payroll].[CustomerVarCostMappings] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [Deleted]        BIT           NOT NULL,
    [DeletedDate]    DATETIME      NULL,
    [Inactive]       BIT           NOT NULL,
    [InactiveDate]   DATETIME      NULL,
    [ActivityTypeId] INT           NOT NULL,
    [LocationCode]   NVARCHAR (10) NULL,
    [CostUnitId]     NVARCHAR (10) NULL,
    [TimeType]     NVARCHAR (10) NULL
);

CREATE CLUSTERED INDEX [IX_CustomerVarCostMappings]
	ON [payroll].[CustomerVarCostMappings]
	(Id);



INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 6,'OFF','700',  'ORD')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 12,'BASE', '700', 'VELFERD')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 1,'OFF','700',  'ORD')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 7,'OFFUTL','700',  'ORD')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 3,'BASE','700',  'ORD')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 8,'UTL','700',  'ORD')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 4,'BASE','700',  'ORD')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 18,'OFF','700',  'ORD')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 11,'BASE', '700', 'PERMUL�NN')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 10,'BASE', '700', 'SYK')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 14,'BASE','700',  'KURS')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 17,'OFF','700',  'ORD')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 13,'OFF','700',  'REISKOMP')
INSERT INTO payroll.CustomerVarCostMappings (Deleted, Inactive, ActivityTypeId, LocationCode, CostUnitId, TimeType) VALUES ('False', 'False', 9,'BASE', '700', 'FERIE')
