﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18047
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PerseusIkmExport
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="IKM")]
	public partial class IKMDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertVariableCostMonth(VariableCostMonth instance);
    partial void UpdateVariableCostMonth(VariableCostMonth instance);
    partial void DeleteVariableCostMonth(VariableCostMonth instance);
    partial void InsertPersonDay(PersonDay instance);
    partial void UpdatePersonDay(PersonDay instance);
    partial void DeletePersonDay(PersonDay instance);
    #endregion
		
		public IKMDataContext() : 
				base(global::PerseusIkmExport.Properties.Settings.Default.IKMConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public IKMDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public IKMDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public IKMDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public IKMDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<VariableCostMonth> VariableCostMonths
		{
			get
			{
				return this.GetTable<VariableCostMonth>();
			}
		}
		
		public System.Data.Linq.Table<PersonDay> PersonDays
		{
			get
			{
				return this.GetTable<PersonDay>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="payroll.VariableCostMonths")]
	public partial class VariableCostMonth : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Id;
		
		private bool _Deleted;
		
		private System.Nullable<System.DateTime> _DeletedDate;
		
		private bool _Inactive;
		
		private System.Nullable<System.DateTime> _InactiveDate;
		
		private int _PersonId;
		
		private int _Year;
		
		private int _Month;
		
		private int _StatusId;
		
		private bool _IsLocked;
		
		private System.DateTime _Created;
		
		private System.Nullable<System.DateTime> _Modified;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(int value);
    partial void OnIdChanged();
    partial void OnDeletedChanging(bool value);
    partial void OnDeletedChanged();
    partial void OnDeletedDateChanging(System.Nullable<System.DateTime> value);
    partial void OnDeletedDateChanged();
    partial void OnInactiveChanging(bool value);
    partial void OnInactiveChanged();
    partial void OnInactiveDateChanging(System.Nullable<System.DateTime> value);
    partial void OnInactiveDateChanged();
    partial void OnPersonIdChanging(int value);
    partial void OnPersonIdChanged();
    partial void OnYearChanging(int value);
    partial void OnYearChanged();
    partial void OnMonthChanging(int value);
    partial void OnMonthChanged();
    partial void OnStatusIdChanging(int value);
    partial void OnStatusIdChanged();
    partial void OnIsLockedChanging(bool value);
    partial void OnIsLockedChanged();
    partial void OnCreatedChanging(System.DateTime value);
    partial void OnCreatedChanged();
    partial void OnModifiedChanging(System.Nullable<System.DateTime> value);
    partial void OnModifiedChanged();
    #endregion
		
		public VariableCostMonth()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Deleted", DbType="Bit NOT NULL")]
		public bool Deleted
		{
			get
			{
				return this._Deleted;
			}
			set
			{
				if ((this._Deleted != value))
				{
					this.OnDeletedChanging(value);
					this.SendPropertyChanging();
					this._Deleted = value;
					this.SendPropertyChanged("Deleted");
					this.OnDeletedChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DeletedDate", DbType="DateTime")]
		public System.Nullable<System.DateTime> DeletedDate
		{
			get
			{
				return this._DeletedDate;
			}
			set
			{
				if ((this._DeletedDate != value))
				{
					this.OnDeletedDateChanging(value);
					this.SendPropertyChanging();
					this._DeletedDate = value;
					this.SendPropertyChanged("DeletedDate");
					this.OnDeletedDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Inactive", DbType="Bit NOT NULL")]
		public bool Inactive
		{
			get
			{
				return this._Inactive;
			}
			set
			{
				if ((this._Inactive != value))
				{
					this.OnInactiveChanging(value);
					this.SendPropertyChanging();
					this._Inactive = value;
					this.SendPropertyChanged("Inactive");
					this.OnInactiveChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_InactiveDate", DbType="DateTime")]
		public System.Nullable<System.DateTime> InactiveDate
		{
			get
			{
				return this._InactiveDate;
			}
			set
			{
				if ((this._InactiveDate != value))
				{
					this.OnInactiveDateChanging(value);
					this.SendPropertyChanging();
					this._InactiveDate = value;
					this.SendPropertyChanged("InactiveDate");
					this.OnInactiveDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PersonId", DbType="Int NOT NULL")]
		public int PersonId
		{
			get
			{
				return this._PersonId;
			}
			set
			{
				if ((this._PersonId != value))
				{
					this.OnPersonIdChanging(value);
					this.SendPropertyChanging();
					this._PersonId = value;
					this.SendPropertyChanged("PersonId");
					this.OnPersonIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Year", DbType="Int NOT NULL")]
		public int Year
		{
			get
			{
				return this._Year;
			}
			set
			{
				if ((this._Year != value))
				{
					this.OnYearChanging(value);
					this.SendPropertyChanging();
					this._Year = value;
					this.SendPropertyChanged("Year");
					this.OnYearChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Month", DbType="Int NOT NULL")]
		public int Month
		{
			get
			{
				return this._Month;
			}
			set
			{
				if ((this._Month != value))
				{
					this.OnMonthChanging(value);
					this.SendPropertyChanging();
					this._Month = value;
					this.SendPropertyChanged("Month");
					this.OnMonthChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_StatusId", DbType="Int NOT NULL")]
		public int StatusId
		{
			get
			{
				return this._StatusId;
			}
			set
			{
				if ((this._StatusId != value))
				{
					this.OnStatusIdChanging(value);
					this.SendPropertyChanging();
					this._StatusId = value;
					this.SendPropertyChanged("StatusId");
					this.OnStatusIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IsLocked", DbType="Bit NOT NULL")]
		public bool IsLocked
		{
			get
			{
				return this._IsLocked;
			}
			set
			{
				if ((this._IsLocked != value))
				{
					this.OnIsLockedChanging(value);
					this.SendPropertyChanging();
					this._IsLocked = value;
					this.SendPropertyChanged("IsLocked");
					this.OnIsLockedChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Created", DbType="DateTime NOT NULL")]
		public System.DateTime Created
		{
			get
			{
				return this._Created;
			}
			set
			{
				if ((this._Created != value))
				{
					this.OnCreatedChanging(value);
					this.SendPropertyChanging();
					this._Created = value;
					this.SendPropertyChanged("Created");
					this.OnCreatedChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Modified", DbType="DateTime")]
		public System.Nullable<System.DateTime> Modified
		{
			get
			{
				return this._Modified;
			}
			set
			{
				if ((this._Modified != value))
				{
					this.OnModifiedChanging(value);
					this.SendPropertyChanging();
					this._Modified = value;
					this.SendPropertyChanged("Modified");
					this.OnModifiedChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="pts.PersonDays")]
	public partial class PersonDay : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Id;
		
		private bool _Deleted;
		
		private System.Nullable<System.DateTime> _DeletedDate;
		
		private bool _Inactive;
		
		private System.Nullable<System.DateTime> _InactiveDate;
		
		private System.DateTime _Day;
		
		private int _PersonId;
		
		private System.Nullable<int> _ActivityId;
		
		private System.Nullable<int> _ShiftId;
		
		private int _ScheduledType;
		
		private double _CalculatedOvertime;
		
		private double _CalculatedManYearHours;
		
		private double _ExpectedManYearHours;
		
		private System.Nullable<double> _ExtraOvertime;
		
		private System.Nullable<double> _ManualHours;
		
		private System.Nullable<double> _ManualOvertime;
		
		private System.Nullable<double> _ManualManYearHours;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(int value);
    partial void OnIdChanged();
    partial void OnDeletedChanging(bool value);
    partial void OnDeletedChanged();
    partial void OnDeletedDateChanging(System.Nullable<System.DateTime> value);
    partial void OnDeletedDateChanged();
    partial void OnInactiveChanging(bool value);
    partial void OnInactiveChanged();
    partial void OnInactiveDateChanging(System.Nullable<System.DateTime> value);
    partial void OnInactiveDateChanged();
    partial void OnDayChanging(System.DateTime value);
    partial void OnDayChanged();
    partial void OnPersonIdChanging(int value);
    partial void OnPersonIdChanged();
    partial void OnActivityIdChanging(System.Nullable<int> value);
    partial void OnActivityIdChanged();
    partial void OnShiftIdChanging(System.Nullable<int> value);
    partial void OnShiftIdChanged();
    partial void OnScheduledTypeChanging(int value);
    partial void OnScheduledTypeChanged();
    partial void OnCalculatedOvertimeChanging(double value);
    partial void OnCalculatedOvertimeChanged();
    partial void OnCalculatedManYearHoursChanging(double value);
    partial void OnCalculatedManYearHoursChanged();
    partial void OnExpectedManYearHoursChanging(double value);
    partial void OnExpectedManYearHoursChanged();
    partial void OnExtraOvertimeChanging(System.Nullable<double> value);
    partial void OnExtraOvertimeChanged();
    partial void OnManualHoursChanging(System.Nullable<double> value);
    partial void OnManualHoursChanged();
    partial void OnManualOvertimeChanging(System.Nullable<double> value);
    partial void OnManualOvertimeChanged();
    partial void OnManualManYearHoursChanging(System.Nullable<double> value);
    partial void OnManualManYearHoursChanged();
    #endregion
		
		public PersonDay()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Deleted", DbType="Bit NOT NULL")]
		public bool Deleted
		{
			get
			{
				return this._Deleted;
			}
			set
			{
				if ((this._Deleted != value))
				{
					this.OnDeletedChanging(value);
					this.SendPropertyChanging();
					this._Deleted = value;
					this.SendPropertyChanged("Deleted");
					this.OnDeletedChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DeletedDate", DbType="DateTime")]
		public System.Nullable<System.DateTime> DeletedDate
		{
			get
			{
				return this._DeletedDate;
			}
			set
			{
				if ((this._DeletedDate != value))
				{
					this.OnDeletedDateChanging(value);
					this.SendPropertyChanging();
					this._DeletedDate = value;
					this.SendPropertyChanged("DeletedDate");
					this.OnDeletedDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Inactive", DbType="Bit NOT NULL")]
		public bool Inactive
		{
			get
			{
				return this._Inactive;
			}
			set
			{
				if ((this._Inactive != value))
				{
					this.OnInactiveChanging(value);
					this.SendPropertyChanging();
					this._Inactive = value;
					this.SendPropertyChanged("Inactive");
					this.OnInactiveChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_InactiveDate", DbType="DateTime")]
		public System.Nullable<System.DateTime> InactiveDate
		{
			get
			{
				return this._InactiveDate;
			}
			set
			{
				if ((this._InactiveDate != value))
				{
					this.OnInactiveDateChanging(value);
					this.SendPropertyChanging();
					this._InactiveDate = value;
					this.SendPropertyChanged("InactiveDate");
					this.OnInactiveDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Day", DbType="DateTime NOT NULL")]
		public System.DateTime Day
		{
			get
			{
				return this._Day;
			}
			set
			{
				if ((this._Day != value))
				{
					this.OnDayChanging(value);
					this.SendPropertyChanging();
					this._Day = value;
					this.SendPropertyChanged("Day");
					this.OnDayChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PersonId", DbType="Int NOT NULL")]
		public int PersonId
		{
			get
			{
				return this._PersonId;
			}
			set
			{
				if ((this._PersonId != value))
				{
					this.OnPersonIdChanging(value);
					this.SendPropertyChanging();
					this._PersonId = value;
					this.SendPropertyChanged("PersonId");
					this.OnPersonIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ActivityId", DbType="Int")]
		public System.Nullable<int> ActivityId
		{
			get
			{
				return this._ActivityId;
			}
			set
			{
				if ((this._ActivityId != value))
				{
					this.OnActivityIdChanging(value);
					this.SendPropertyChanging();
					this._ActivityId = value;
					this.SendPropertyChanged("ActivityId");
					this.OnActivityIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ShiftId", DbType="Int")]
		public System.Nullable<int> ShiftId
		{
			get
			{
				return this._ShiftId;
			}
			set
			{
				if ((this._ShiftId != value))
				{
					this.OnShiftIdChanging(value);
					this.SendPropertyChanging();
					this._ShiftId = value;
					this.SendPropertyChanged("ShiftId");
					this.OnShiftIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ScheduledType", DbType="Int NOT NULL")]
		public int ScheduledType
		{
			get
			{
				return this._ScheduledType;
			}
			set
			{
				if ((this._ScheduledType != value))
				{
					this.OnScheduledTypeChanging(value);
					this.SendPropertyChanging();
					this._ScheduledType = value;
					this.SendPropertyChanged("ScheduledType");
					this.OnScheduledTypeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CalculatedOvertime", DbType="Float NOT NULL")]
		public double CalculatedOvertime
		{
			get
			{
				return this._CalculatedOvertime;
			}
			set
			{
				if ((this._CalculatedOvertime != value))
				{
					this.OnCalculatedOvertimeChanging(value);
					this.SendPropertyChanging();
					this._CalculatedOvertime = value;
					this.SendPropertyChanged("CalculatedOvertime");
					this.OnCalculatedOvertimeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CalculatedManYearHours", DbType="Float NOT NULL")]
		public double CalculatedManYearHours
		{
			get
			{
				return this._CalculatedManYearHours;
			}
			set
			{
				if ((this._CalculatedManYearHours != value))
				{
					this.OnCalculatedManYearHoursChanging(value);
					this.SendPropertyChanging();
					this._CalculatedManYearHours = value;
					this.SendPropertyChanged("CalculatedManYearHours");
					this.OnCalculatedManYearHoursChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ExpectedManYearHours", DbType="Float NOT NULL")]
		public double ExpectedManYearHours
		{
			get
			{
				return this._ExpectedManYearHours;
			}
			set
			{
				if ((this._ExpectedManYearHours != value))
				{
					this.OnExpectedManYearHoursChanging(value);
					this.SendPropertyChanging();
					this._ExpectedManYearHours = value;
					this.SendPropertyChanged("ExpectedManYearHours");
					this.OnExpectedManYearHoursChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ExtraOvertime", DbType="Float")]
		public System.Nullable<double> ExtraOvertime
		{
			get
			{
				return this._ExtraOvertime;
			}
			set
			{
				if ((this._ExtraOvertime != value))
				{
					this.OnExtraOvertimeChanging(value);
					this.SendPropertyChanging();
					this._ExtraOvertime = value;
					this.SendPropertyChanged("ExtraOvertime");
					this.OnExtraOvertimeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ManualHours", DbType="Float")]
		public System.Nullable<double> ManualHours
		{
			get
			{
				return this._ManualHours;
			}
			set
			{
				if ((this._ManualHours != value))
				{
					this.OnManualHoursChanging(value);
					this.SendPropertyChanging();
					this._ManualHours = value;
					this.SendPropertyChanged("ManualHours");
					this.OnManualHoursChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ManualOvertime", DbType="Float")]
		public System.Nullable<double> ManualOvertime
		{
			get
			{
				return this._ManualOvertime;
			}
			set
			{
				if ((this._ManualOvertime != value))
				{
					this.OnManualOvertimeChanging(value);
					this.SendPropertyChanging();
					this._ManualOvertime = value;
					this.SendPropertyChanged("ManualOvertime");
					this.OnManualOvertimeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ManualManYearHours", DbType="Float")]
		public System.Nullable<double> ManualManYearHours
		{
			get
			{
				return this._ManualManYearHours;
			}
			set
			{
				if ((this._ManualManYearHours != value))
				{
					this.OnManualManYearHoursChanging(value);
					this.SendPropertyChanging();
					this._ManualManYearHours = value;
					this.SendPropertyChanged("ManualManYearHours");
					this.OnManualManYearHoursChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
