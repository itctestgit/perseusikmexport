﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using perseus.context;

namespace PerseusIkmExport
{
    internal class PerseusIkmTimeListExport
    {
        private static readonly string ErrorLogFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                                      @"\" + DateTime.Today.ToString("yyyyMM") + ".log";

        private static readonly string WorkLogfile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                                 @"\work.log";



        //Liten endring for å teste en git commit lokalt, commit nr 1

        //mer endring, commit nr 2


        //Denne skal pushes og commites

        /// <summary>
        /// Get updated data from Perseus and insert into IKM database SOLA-SQL4
        /// </summary>
        private static void Main()
        {
            try
            {
                ProtectSection("appSettings");

                using (var sourcecontext = new HrEntities(ConfigurationManager.AppSettings["SourceConnectionString"]))
                using (var targetcontext = new IkmContext())
                {
                    //Save time at start, so any data added during run is not missed next time
                    DateTime startTime = DateTime.Now;

                    //Get list of updated or created timelists from Perseus database
                    //try
                    //{
                        List<ExportRecord> timelists = GetTimeListsQuery(sourcecontext, LastRun()).ToList();
                        WriteToLog("Normal time records found: " + timelists.Count.ToString(CultureInfo.CurrentCulture));
                        if (timelists.Count > 0)
                            targetcontext.TimeLists.InsertAllOnSubmit(timelists);
                    //}
                    //catch (Exception ex)
                    //{
                    //    WriteToLog("Error getting timelists: ");
                    //    WriteToLog(ex.ToString());
                    //}

                    //Get list of overtime as separate list, Perseus uses same field for overtime and normal time
                    //try
                    //{
                        List<ExportRecord> overtimelists = GetOverTimeListsQuery(sourcecontext, LastRun()).ToList();
                        WriteToLog("Overtime records found: " + overtimelists.Count.ToString(CultureInfo.CurrentCulture));
                        if (overtimelists.Count > 0)
                            targetcontext.TimeLists.InsertAllOnSubmit(overtimelists);
                    //}
                    //catch (Exception ex)
                    //{
                    //    WriteToLog("Error getting overtimelists: ");
                    //    WriteToLog(ex.ToString());
                    //}

                    //Get list of variable costs
                    //try
                    //{
                        List<ExportRecord> variablecostList =
                            GetVariableCostListsQuery(sourcecontext, LastRun()).ToList();
                        WriteToLog("Variablecost records found: " + variablecostList.Count.ToString(CultureInfo.CurrentCulture));
                        if (variablecostList.Count > 0)
                            targetcontext.TimeLists.InsertAllOnSubmit(variablecostList);
                    //}
                    //catch (Exception ex)
                    //{
                    //    WriteToLog("Error getting variablecostList: ");
                    //    WriteToLog(ex.ToString());
                    //}

                    //submit to database
                    targetcontext.SubmitChanges();
                    LogSuccessfulRun(startTime);
                }
            } 
            catch(ReflectionTypeLoadException e) {
                var exceptions = "The following DLL load exceptions occurred:";
                foreach(var x in e.LoaderExceptions) {
                    exceptions += x.Message + ",\n\n";
                }
                throw new Exception("Error loading DLL:\n" + exceptions);
            }
            catch (Exception ex)
            {
                WriteToLog(ex.ToString());
            }
        }

        /// <summary>
        /// All persondays from updated/created variablecostmonths
        /// Days with 0 work are also included so deleted days are updated
        /// </summary>
        /// <param name="context"> </param>
        /// <param name="lastRun"></param>
        /// <returns></returns>
        private static IEnumerable<ExportRecord> GetTimeListsQuery(HrEntities context, DateTime lastRun)
        {
            //Check what months are updated since last run, get days from persondays for the updated month 
            //Join to get schedule and rotation data. Get rotation in use on the given day. 
            //Deafult values are added to avoid problems when inserting days without activity
            return from personday in context.PersonDays
                   join costmonth in context.VariableCostMonths on personday.PersonId equals costmonth.PersonId
                   where ((costmonth.Modified ?? costmonth.Created) > lastRun &&
                          costmonth.Year == personday.Day.Year &&
                          costmonth.Month == personday.Day.Month)
                   join rotation in context.PersonSchedules on personday.PersonId equals rotation.PersonId
                   where (rotation.StartDate <= personday.Day &&
                          (rotation.EndDate == null || rotation.EndDate >= personday.Day))
                   join costmap in context.CustomerVarCostMappings on personday.Activity.ActivityTypeId equals costmap.ActivityTypeId
                       into activityandcost
                   //Left outer join to get days without any activity
                   from activitycost in activityandcost.DefaultIfEmpty()
                   select new ExportRecord
                              {
                                  WORK_DATE = personday.Day,
                                  EMPLOYEE_ID = personday.Person.ErpId.Trim().Substring(0, 10),
                                  COST_UNIT_ID = string.IsNullOrEmpty((personday.Activity.Demand.Job.CustomerRef ?? activitycost.CostUnitId)) 
                                                 ? "700" : (personday.Activity.Demand.Job.CustomerRef ?? activitycost.CostUnitId).Substring(0, 15),
                                  LOCATION_CODE = string.IsNullOrEmpty(activitycost.LocationCode) ? "BASE" : activitycost.LocationCode.Substring(0, 10),
                                  ROTATION_ID = rotation.Schedule.Name.Substring(0, 10),
                                  SHIFT_CODE = string.IsNullOrEmpty(personday.Shift.Name) ? "Day" : personday.Shift.Name.Substring(0, 10),
                                  HOURS_TYPE_CODE = string.IsNullOrEmpty(activitycost.TimeType) ? "ORD" : activitycost.TimeType.Substring(0, 10),
                                  HOURS = (personday.ManualManYearHours ?? personday.CalculatedManYearHours)
                              };
        }

        /// <summary>
        /// Days with overtime from updated/created variablecostmonths
        /// </summary>
        /// <param name="context"> </param>
        /// <param name="lastRun"></param>
        /// <returns></returns>
        private static IEnumerable<ExportRecord> GetOverTimeListsQuery(HrEntities context, DateTime lastRun)
        {
            //Check what months are updated since last run, get days from persondays for the updated month 
            //Join to get schedule and rotation data. Get rotation in use on the given day. 
            //Only days with overtime
            //If no shift is used, the default value is "Day" to have the required value to insert
            return from personday in context.PersonDays
                   join costmonth in context.VariableCostMonths
                       on personday.PersonId equals costmonth.PersonId
                   where ((costmonth.Modified ?? costmonth.Created) > lastRun &&
                          costmonth.Year == personday.Day.Year &&
                          costmonth.Month == personday.Day.Month &&
                          (personday.ManualOvertime ?? personday.CalculatedOvertime) > 0)
                   join rotation in context.PersonSchedules
                       on personday.PersonId equals rotation.PersonId
                   where (rotation.StartDate <= personday.Day &&
                          (rotation.EndDate == null || rotation.EndDate >= personday.Day))
                   join costmap in context.CustomerVarCostMappings
                       on personday.Activity.ActivityTypeId equals costmap.ActivityTypeId
                   select new ExportRecord
                              {
                                  WORK_DATE = personday.Day,
                                  EMPLOYEE_ID = personday.Person.ErpId.Trim().Substring(0, 10),
                                  COST_UNIT_ID = (personday.Activity.Demand.Job.CustomerRef ?? costmap.CostUnitId).Substring(0, 15),
                                  LOCATION_CODE = costmap.LocationCode.Substring(0, 10),
                                  ROTATION_ID = rotation.Schedule.Name.Substring(0, 10),
                                  SHIFT_CODE = string.IsNullOrEmpty(personday.Shift.Name) ? "Day" : personday.Shift.Name.Substring(0, 10),
                                  HOURS_TYPE_CODE = "OVT065",
                                  HOURS = (personday.ManualOvertime ?? personday.CalculatedOvertime)
                              };
        }

        /// <summary>
        /// Variable cost from updated/created variablecostmonths
        /// </summary>
        /// <param name="context"> </param>
        /// <param name="lastRun"></param>
        /// <returns></returns>
        private static IEnumerable<ExportRecord> GetVariableCostListsQuery(HrEntities context, DateTime lastRun)
        {
                    //Get variable cost entries from updated or created months
            return from varcostentry in context.VariableCostEntries
                   join month in context.VariableCostMonths
                       on varcostentry.CostMonthId equals month.Id
                   where ((month.Modified ?? month.Created) > lastRun)
                   where (varcostentry.ManualQuantity ?? varcostentry.Quantity) > 0

                   //Get shift type one the given date
                   join rotation in context.PersonSchedules
                       on month.PersonId equals rotation.PersonId
                       into testSchedule
                   from s in testSchedule
                   where (s.StartDate <= EntityFunctions.CreateDateTime(month.Year, month.Month, varcostentry.Day, 0, 0, 0)) &&
                       (s.EndDate == null || s.EndDate >= EntityFunctions.CreateDateTime(month.Year, month.Month, varcostentry.Day, 0, 0, 0))

                   //Get HOURS_TYPE_CODE
                   join costtype in context.VariableCostTypes
                       on varcostentry.CostTypeId equals costtype.Id

                   //Get SHIFT_CODE
                   join personday in context.PersonDays
                       on month.PersonId equals personday.PersonId
                   where
                       personday.Day == EntityFunctions.CreateDateTime(month.Year, month.Month, varcostentry.Day, 0, 0, 0)

                   //Get COST_UNIT_ID and LOCATION_CODE	

                   //TODO not all activitytypes are mapped in IkmVarCostMappings, update and then remove "into+++"
                   join costmap in context.CustomerVarCostMappings
                       on personday.Activity.ActivityTypeId equals costmap.ActivityTypeId
                       //into costmapping
                   //from cm in costmapping.DefaultIfEmpty()
                   select new ExportRecord
                              {
                                  WORK_DATE = (DateTime) EntityFunctions.CreateDateTime(month.Year, month.Month, varcostentry.Day, 0, 0, 0),
                                  EMPLOYEE_ID = month.Person.ErpId.Trim().Substring(0, 10),
                                  COST_UNIT_ID = string.IsNullOrEmpty((personday.Activity.Demand.Job.CustomerRef ?? costmap.CostUnitId))
                                            ? "700" : (personday.Activity.Demand.Job.CustomerRef ?? costmap.CostUnitId).Substring(0, 15),
                                  LOCATION_CODE = string.IsNullOrEmpty(costmap.LocationCode) 
                                            ? "BASE" : costmap.LocationCode.Substring(0, 10),
                                  ROTATION_ID = s.Schedule.Name.Substring(0, 10),
                                  SHIFT_CODE = string.IsNullOrEmpty(personday.Shift.Name) 
                                            ? "Day" : personday.Shift.Name.Substring(0, 10),
                                  HOURS_TYPE_CODE = costtype.Name.Substring(0, 10),
                                  HOURS = (double) (varcostentry.ManualQuantity ?? varcostentry.Quantity)
                              };
        }

        /// <summary>
        /// Read worklog, get last runtime
        /// </summary>
        /// <returns></returns>
        private static DateTime LastRun()
        {
            DateTime lastRun;
            if (File.Exists(WorkLogfile))
            {
                var sr = new StreamReader(WorkLogfile, true);
                lastRun = DateTime.Parse(sr.ReadLine(), CultureInfo.CurrentCulture);
                sr.Close();
            }
            else 
                lastRun = new DateTime(2013, 1, 1); //TODO Change this?
            return lastRun;
        }

        /// <summary>
        /// Log successful run time
        /// </summary>
        /// <param name="startTime"> </param>
        private static void LogSuccessfulRun(DateTime startTime)
        {
            if (File.Exists(WorkLogfile))
                File.Delete(WorkLogfile);
            var sw = new StreamWriter(File.Create(WorkLogfile));
            sw.WriteLine(startTime.ToString(CultureInfo.CurrentCulture));
            sw.Close();
        }

        /// <summary>
        /// Write to log, with timestamp
        /// </summary>
        /// <param name="str"></param>
        private static void WriteToLog(string str)
        {
            var sw = File.Exists(ErrorLogFile)
                    ? new StreamWriter(ErrorLogFile, true)
                    : new StreamWriter(File.Create(ErrorLogFile));
            sw.WriteLine(DateTime.Now.ToString("h:mm:ss  dd/MM/yyyy ") + str);
            sw.Close();
        }

        /// <summary>
        /// Encrypt section of config file
        /// </summary>
        /// <param name="sectionName"></param>
        private static void ProtectSection(String sectionName)
        {
            // Open the app.config file.
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            // Get the section in the file.
            ConfigurationSection section = config.GetSection(sectionName);
            // If the section exists and the section is not readonly, then protect the section.
            if (section != null && !section.IsReadOnly())
            {
                // Protect the section.
                section.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                section.SectionInformation.ForceSave = true;
                // Save the change.
                config.Save(ConfigurationSaveMode.Full);
            }
        }
    }
}