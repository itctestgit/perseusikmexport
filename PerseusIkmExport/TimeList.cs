﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PerseusIkmExport
{
    //Table name in IKM database IKM_SUB_HRS500_HOURS_IMP_TAB
    // View name in IKM database IKM_SUB_HRS500_HOURS_IMP_V1
    //TODO endre til å bruke view. 
    [System.Data.Linq.Mapping.Table(Name = "IKM_SUB_HRS500_HOURS_IMP_V1")]
    internal class ExportRecord
    {
        [System.Data.Linq.Mapping.Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int TRANSACTION_ID { get; set; }

        [System.Data.Linq.Mapping.Column]
        public DateTime WORK_DATE { get; set; }

        [System.Data.Linq.Mapping.Column]
        [MaxLength(10)]
        public string EMPLOYEE_ID { get; set; }

        [System.Data.Linq.Mapping.Column]
        [MaxLength(15)]
        public string COST_UNIT_ID { get; set; }

        [System.Data.Linq.Mapping.Column]
        [MaxLength(10)]
        public string LOCATION_CODE { get; set; }

        [System.Data.Linq.Mapping.Column]
        [MaxLength(10)]
        public string ROTATION_ID { get; set; }

        [System.Data.Linq.Mapping.Column]
        [MaxLength(10)]
        public string SHIFT_CODE { get; set; }

        [System.Data.Linq.Mapping.Column]
        [MaxLength(10)]
        public string HOURS_TYPE_CODE { get; set; }

        [System.Data.Linq.Mapping.Column]
        public double HOURS { get; set; }
    }
}